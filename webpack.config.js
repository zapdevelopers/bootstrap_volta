const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path');

module.exports = {
  entry: {
    global: './src/index.js',
  },
  output: {
    path: __dirname,
    filename: 'js/[name].js'
  },
  externals: {
    jquery: 'jQuery',
    'jquery.once': 'jQuery.once',
    bootstrap: 'bootstrap'
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader, {
            loader: "css-loader" // translates CSS into CommonJS
          }, {
            loader: 'postcss-loader', // Run postcss actions
            options: {
              plugins: function () { // postcss plugins, can be exported to postcss.config.js
                return [
                  require('autoprefixer')
                ];
              }
            }
          }, {
            loader: "sass-loader" // compiles Sass to CSS
          }]
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name].[ext]',
              context: path.resolve(__dirname, "src/images"),
              outputPath: 'images/',
              publicPath: '../images/',
              useRelativePaths: true
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "css/style.css",
      chunkFilename: "css/[name].css"
    })
  ]
}
